from scapy.all import *
from time import strftime
from os.path import isfile

HISTORY_FILE_NAME = "History.txt"


def Update_History(URL):
    """This function updates the history table and file by adding the URL and time of visit(current time)"""
    curr_time = strftime('%H:%M') # Getting the current time
    curr_date = strftime("%d/%m/%Y") # Getting the current date
    history_file = open(HISTORY_FILE_NAME, "a+")  # Opening the file
    history_file.write("\n%s %s %s" % (URL, curr_time, curr_date)) # Writing the new info into the file
    history_file.close() # Closing the file


def Clear_History():
    """This function removes all data from table and file"""
    try:
        history_file = open("History.txt", "w")  # Opening the file
        history_file.truncate() # Cleaning the file
        history_file.close() # Closing the file
    except:
        return False
    return True


def Remove_From_History(URL):
    """This function removes every given url from the history file"""
    try:
        history_file = open(HISTORY_FILE_NAME, "r") # Opening the file
        lines = history_file.readlines() # Copying each line of the file
        history_file.close() # Closing the file

        history_file = open(HISTORY_FILE_NAME, "w") # Opening the file
        for line in lines:
            if line.split(' ')[0] != URL: # If the line isn't the URL
                history_file.write(line) # Writing the line into the file
        history_file.close() # Closing the file
    except:
        return False
    return True


def Print_History():
    """This function will print the recorded search history"""
    try:
        history_file = open(HISTORY_FILE_NAME, 'r') # Opening the file
        lines = history_file.readlines()[::-1] # Reading the lines from the last to first line
        i = 1 # Declaring i - helps numbering each line
        x = len(str(len(lines)))
        print "%-*s | Date and Time    | URL" % (x+1, "No.")
        for line in lines: # For each line in the file
            line = line.split(' ') # Splitting the line
            print "%-3s | %s %s | %s" % (str(i) + '.', line[2][:], line[1], line[0]) # Printing the current cache line
            i += 1 # Increasing i by 1
    except:
        return False
    return True


def History_Menu():
    """This function opens the menu of choices which the user can choose from"""
    if isfile(HISTORY_FILE_NAME):
        choice = raw_input(
            "\n1. Show History\n2. Clear History\n3. Remove A Specific Record From History\nPlease insert your choice: ")
        if choice == '1':
            print
            Print_History() # Printing the history
            print
        elif choice == '2':
            Clear_History() # Clearing the history
        elif choice == '3':
            Print_History() # Printing the history for convenience reasons
            Remove_From_History(raw_input("Please insert the number of the line you want to remove:")) # Removing the line
        else:
            print "Your choice is invalid! Please insert a valid choice.\n"
            History_Menu() # Reactivating the menu
        return True
    else:
        return False


