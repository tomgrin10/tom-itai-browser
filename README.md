Tom-Itay-Browser
===============
Magshimim Networks End-Project

Step 1: ✓
----------------
To be done until Monday 16/5.

1. `Arp_Req(IP)`  ​// returns the MAC address of a given IP address 
2. `DNS_Req(URL)`  ​// Creates a DNS Query to 8.8.8.8 and returns the IP 
3. `Ret_From_Cache(URL)` ​// returns the IP of the URL from the internal DNS table if exists , else – return None. 
4. `Flush_DNS()` ​//This function delete all content of the DNS caching file and the content of the DNS table. 

Step 2: ✓
----------------
To be done until Monday 23/5.

1. `Update_history(URL)` ​// This function updates the history table and file by adding 
the URL and time of visit(current time) 
2. `Clear_History()` ​// This function removes all the data from the history table and file 
3. `Remove_From_History(URL)` ​// This function removes all the records of the 
given URL from the History table and file
4. `Find_IP(URL)` ​// The function uses "Ret_From_Cache" and "DNS_Req" functions to 
find the IP address of a given URL 
5. `History_menu()` //​this function opens a menu which contains the following options 
  1. `Show History`     //print out all the history – line by line 
  2. `Clear History`     //clear the history and print ­ "done"  
  3. `Remove a specific record` //asks the user for a number input. Removes the history with 
that serial number. (1­10) 

Step 3:
----------------
To be done until Monday 30/5.

1. `Menu()`         // ​This function shows the browser's menu which contains the following 
  1. `History`        //opens the history's Menu  
  2. `Visit a site`   // asks for a URL address (uses the func "Make_Get") 
  3. `Credits`        //displays the browser’s and a list of all the creators and contributers4. 
  4. `Exit`           // well…. guess what  
2. `Make_GET(URL)` // gets a URL and solves its IP address. This function uses the "find_ip" function and creates an HTTP GET message to this site. finally sends it and saves the data of the received packet to an html file.