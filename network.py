from scapy.all import *
from time import strftime

DNS_FILE_NAME = "DNS_Records.txt"
FIN = 0x01


def Arp_Req(IP):
    """This function returns the MAC address of a given IP address using the ARP protocol"""
    packet = Ether(dst = "ff:ff:ff:ff:ff:ff") / ARP(pdst = IP, hwdst = 'ff:ff:ff:ff:ff:ff') # Setting the ARP packet
    response = srp1(packet, timeout=2, verbose = False) # Setting the response up
    if response is not None:	# Checks if theres an answer
        return response[0][ARP].hwsrc
    else:
        return None	# returns None if got no answer


def DNS_Req(URL):
    """This function creates a DNS query to 8.8.8.8 and returns the ip"""
    packet = IP(dst="8.8.8.8") / UDP(dport=53) / DNS(rd=1, qd=DNSQR(qname=URL)) # Setting the DNS packet up
    response = sr1(packet, verbose = False) # Setting the response up
    if response[DNS].ancount != 0:  # Checks if theres an answer
        return response[DNS].an[response[DNS].ancount - 1].rdata
    else:
        return None	# returns None if got no answer


def Ret_From_Cache(URL):
    """This function searches for the given URL in the DNS records and returns the ip listed for it."""
    records = open(DNS_FILE_NAME, "r")  # Opening the file
    for record in records.readlines():  # Searches every line
        if record[0] != '#':    # If the line is not a comment
            record = record.split(' ')  # Spliting the cache table - ' ' is the char in between each column
            if record[0] == URL:    # If the cache line matches the searched line
                records.close()     # Closing the file
                return record[1]    # Returning the ip address
    records.close() # Closing the file
    return None


def Flush_DNS():
    """This function deletes the content of the DNS records"""
    records = open(DNS_FILE_NAME, "w")  # Opening the file
    records.truncate() # Cleaning the file
    records.close() # Closing the file


def Find_IP(URL):
    """This function finds the IP of a given URL via Ret_From_Cache and DNS_Req"""
    IP = Ret_From_Cache(URL) # Getting the IP from the cache
    if IP is None: # If the IP is not in the cache
        IP = DNS_Req(URL)
        DNS_file = open(DNS_FILE_NAME, "a+")
        DNS_file.write("%s %s %s\n" % (URL, IP, strftime("%H:%M")))
        DNS_file.close()
    return IP


def SplitURL(URL):
    splitted = URL.split('/')
    domain = splitted[0]
    page = ""
    for i in splitted[1:]:
        page += i
    return (domain, page)


def Make_GET(URL):
    """This function will make an http GET request"""
    try:
        os.system('iptables -A OUTPUT -p tcp --tcp-flags RST RST -s ' + '192.168.44.128' + ' -j DROP')
        URL, page = SplitURL(URL)
        ip = DNS_Req(URL)
        src_port = 22223
        GET = 'GET /%s HTTP/1.1\r\nHost: %s\r\nConnection: close\r\n\r\n' % (page, URL)
    
        syn = IP(dst=ip) / TCP(sport=src_port, dport=80, flags='S')
        ack = IP(dst=ip) / TCP(sport=src_port, dport=80, flags='A') / GET
    
        syn_ack = sr1(syn, verbose=False, timeout=2)[0]
        ack[TCP].seq = syn_ack[TCP].ack
        ack[TCP].ack = syn_ack[TCP].seq + 1
        send(ack, verbose=False)
        ack = IP(dst=ip) / TCP(sport=src_port, dport=80, flags='A')
        response = list()
        while True:
            temp = sniff(lfilter=lambda p: IP in p and p[IP].src==ip, count=1)[0]
            ack[TCP].seq = temp[TCP].ack
            ack[TCP].ack = temp[TCP].seq + temp[IP].len
            send(ack, verbose=False)
            response.append(temp)
            if temp[TCP].flags & FIN:
                break
    
        html_file = open("website.html", "w+")
        for i in range(len(response[0][Raw].load)):
            c = response[0][Raw].load[i]
            if c == '<':
                html_file.write(response[0][Raw].load[i:])
                break
        for i in range(1, len(response)):
            p = response[i]
            if Raw in p:
                html_file.write(p[Raw].load)
        html_file.close()
    except:
        return False
    return True


