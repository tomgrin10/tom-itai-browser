from network import *
from history import *


def Menu():
    """This function shows the browser's menu which contains all of the options"""
    choice = 0
    print "\nWelcome to our web browser, a program which was browses the web by your request!"
    while(choice != '5'):  # While the user hasn't chosen to exit
        choice = raw_input(
            "Please insert your choice:\n1. History\n2. Visit A Site\n3. Credits\n4. Information\n5. Exit\n")
        if choice == '1':   # History
            if not History_Menu():
                print "\nThere is no history yet.\n"
        elif choice == '2':   # Visit site
            URL = raw_input("\nPlease insert a website's URL:\n")
            if Make_GET(URL):
                Update_History(URL)
                print "\nYour website waits for you in the folder.\n"
            else:
                print "\n404 Not Found.\n"
        elif choice == '3':   # Credits
            print "\nDevelopers: Tom Gringauz & Itai Geller\n"
        elif choice == '4':   # Information
            print "\nHistory - shows the history menu. \nVisit A Site - connects to a website. \nCredits - shows the developers. \n"
        elif choice == '5':   # Exit
            print "\nGoodbye! Have A Nice Day!\n"
        else:
            print "Your choice is invalid! Please insert a valid choice.\n"



Menu()





